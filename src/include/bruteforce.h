#ifndef ACTIONTEC_BRUTEFORCE_H
#define ACTIONTEC_BRUTEFORCE_H

#include <string>
#include <vector>

/**
 * Read a wordlist file and and store the data in a vector
 * @param wordlist The wordlist file
 * @return Vector of words
 */
std::vector<std::string> read_wordlist(const std::string &wordlist);

/**
 * Attempt to login to an Actiontec router at the specified address with a vector of potential passwords
 * @param addr
 * @param username
 * @param wordlist
 * @return Password if found, otherwise a blank string
 */
std::string Actiontec_bruteforce(
        const std::string &addr,
        const std::string &username,
        const std::vector<std::string> &wordlist
);

#endif //ACTIONTEC_BRUTEFORCE_H
