#ifndef ACTIONTEC_ACTIONTECH
#define ACTIONTEC_ACTIONTEC_H

#include <string>
#include <curl/curl.h>

#define ACTIONTEC_LOGIN_SUCCESS 1
#define ACTIONTEC_LOGIN_FAILED 0

/**
 * @note This was successfully tested on the GT784WN under firmware version BA-1.0.16
 */
class Actiontec {
public:
    Actiontec();

    explicit Actiontec(const std::string &addr);

    Actiontec(const std::string &addr, const std::string &username, const std::string &password);

    const std::string &get_addr() const;

    void set_addr(const std::string &addr);

    const std::string &get_username() const;

    void set_username(const std::string &username);

    const std::string &get_password() const;

    void set_password(const std::string &password);

    /**
     * @brief Makes an attempt to login to the router
     * @return Status (0 for denied, 1 for success, and -1 for an error)
     */
    int login() const;

    /**
     * @brief Performs an HTTP GET request to the router's index page
     * @return The string buffer that got written to by libcurl
     */
    std::string get_index() const;

private:
    void set_curl_options(CURL *handle);

    std::string addr_, username_, password_;
    CURL *master_handle_;
};

Actiontec* make_Actiontec();

static size_t write_callback(void *contents, size_t size, size_t nmemb, void *userp);

int Actiontec_login(const std::string &addr, const std::string &username, const std::string &password);

int Actiontec_login(const Actiontec &actiontec);

#endif //ACTIONTEC_ACTIONTEC_H
