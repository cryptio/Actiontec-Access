#include "include/bruteforce.h"
#include "include/actiontec.h"
#include <fstream>
#include <future>
#include <chrono>

std::vector<std::string> read_wordlist(const std::string &wordlist) {
    std::ifstream wordlist_file(wordlist.c_str());
    if (!wordlist_file) {
        perror(wordlist.c_str());
    }

    std::vector<std::string> wordlist_results;
    std::string buffer;
    while (std::getline(wordlist_file, buffer)) {
        wordlist_results.push_back(buffer);
    }
    wordlist_file.close();

    return wordlist_results;
}


std::string Actiontec_bruteforce(
        const std::string &addr,
        const std::string &username,
        const std::vector<std::string> &wordlist
) {
    for (const auto &line : wordlist) {
        /* std::launch::async - create new thread to execute the task asynchronously
         * use lambda function to tell the compiler which overloaded function we want
         */
        const auto timeout = std::chrono::seconds(10);
        std::future<int> ret = std::async(std::launch::async, [&]() { return Actiontec_login(addr, username, line); });
        std::future_status status = ret.wait_for(timeout); // block until result is available
        switch (status) {
            case std::future_status::ready: {   // the thread is ready to give us a value
                const int result = ret.get();
                if (result == ACTIONTEC_LOGIN_SUCCESS) {
                    return line;
                }
                break;
            }
            case std::future_status::timeout:
                throw std::runtime_error("Thread timed out");
            default:
                throw std::runtime_error("Unknown status");

        }
    }

    return "";
}