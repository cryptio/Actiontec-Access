#include "include/actiontec.h"
#include <sstream>

Actiontec::Actiontec()
        : addr_(), master_handle_(curl_easy_init()) {
    set_curl_options(master_handle_);
}

Actiontec::Actiontec(const std::string &addr)
        : addr_(addr), master_handle_(curl_easy_init()) {
    set_curl_options(master_handle_);
}

Actiontec::Actiontec(const std::string &addr, const std::string &username, const std::string &password)
        : addr_(addr), username_(username), password_(password), master_handle_(curl_easy_init()) {
    set_curl_options(master_handle_);
}

void Actiontec::set_curl_options(CURL *handle) {
    curl_easy_setopt(master_handle_, CURLOPT_COOKIEFILE, "");
    curl_easy_setopt(master_handle_, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(master_handle_, CURLOPT_WRITEFUNCTION, write_callback);   // string conversion
}

const std::string &Actiontec::get_addr() const {
    return addr_;
}

void Actiontec::set_addr(const std::string &addr) {
    Actiontec::addr_ = addr;
}

const std::string &Actiontec::get_username() const {
    return username_;
}

void Actiontec::set_username(const std::string &username) {
    Actiontec::username_ = username;
}

const std::string &Actiontec::get_password() const {
    return password_;
}

void Actiontec::set_password(const std::string &password) {
    Actiontec::password_ = password;
}

static size_t write_callback(void *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string *) userp)->append((char *) contents, size * nmemb);
    return size * nmemb;
}


int Actiontec::login() const {
    CURL *handle = curl_easy_duphandle(master_handle_);
    if (!handle) {
        throw std::runtime_error("CURL handle is invalid");
    }

    CURLcode code;
    std::stringstream login_url;
    login_url << "http://" << addr_ << "/login.cgi?adminUserName=" << username_ << "&adminPassword=" << password_
              << "&nothankyou=1&sessionKey=123";

    std::string buffer;
    curl_easy_setopt(handle, CURLOPT_URL, login_url.str().c_str());
    // enable cookies, making it understand and parse received cookies and then use matching cookies in future requests
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &buffer);
    code = curl_easy_perform(handle);
    curl_easy_cleanup(handle);

    if (buffer.empty()) {
        throw std::runtime_error("Read empty buffer");
    }
    if (code == CURLE_OK) { // HTTP status 200
        const std::string login_failed_string = "Login Failed";
        const bool login_success = (buffer.find(login_failed_string) == std::string::npos);
        if (login_success) {
            return ACTIONTEC_LOGIN_SUCCESS;
        } else {
            return ACTIONTEC_LOGIN_FAILED;
        }
    } else {
        std::stringstream curl_err;
        curl_err << "curl_easy_perform() failed" << curl_easy_strerror(code);
        throw std::runtime_error(curl_err.str());
    }
}

std::string Actiontec::get_index() const {
    CURL *handle = curl_easy_duphandle(master_handle_);
    if (!handle) {
        throw std::runtime_error("CURL handle is invalid");
    }

    CURLcode code;
    std::stringstream index_url;
    index_url << "http://" << addr_ << "/index_real.html";

    std::string buffer;
    curl_easy_setopt(handle, CURLOPT_URL, index_url.str().c_str());
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &buffer);
    code = curl_easy_perform(handle);
    curl_easy_cleanup(handle);

    if (code == CURLE_OK) {
        return buffer;
    } else {
        std::stringstream curl_err;
        curl_err << "curl_easy_perform() failed" << curl_easy_strerror(code);
        throw std::runtime_error(curl_err.str());
    }
}

Actiontec *make_Actiontec() {
    return new Actiontec;
}

int Actiontec_login(const std::string &addr, const std::string &username, const std::string &password) {
    Actiontec actiontec(addr, username, password);
    const int ret = actiontec.login();

    return ret;
}

int Actiontec_login(const Actiontec &actiontec) {
    const int ret = actiontec.login();

    return ret;
}