# Actiontec Access
Provides a C++ interface for accessing Actiontec routers over HTTP

## Concept
Unfortunatetly, Actiontec web logins can easily be accessed programatically. The sessionKey field is never checked, there is no limit to how many logins you can attempt, and no other security mechanisms to prevent bruteforce are in place. A WAN-enabled Actiontec router could easily be compromised, which is why it is important that router vendors take the extra time into making sure that their devices can be abused.

## Device coverage
| Model | Compatability |
| ------ | ------ |
| GT784WN | Working |
